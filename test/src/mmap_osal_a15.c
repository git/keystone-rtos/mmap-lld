#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

void  Osal_mmapRegisterRead (int fd, uint32_t *val, uint32_t count, uint32_t offset)
{
  pread(fd,val,count*sizeof(uint32_t),(off_t)offset);
}

void  Osal_mmapRegisterWrite (int fd, uint32_t *val, uint32_t count, uint32_t offset)
{
  pwrite(fd,val,count*sizeof(uint32_t),(off_t)offset);
}

void* Osal_mmapMalloc (uint32_t num_bytes)
{
  return ( malloc(num_bytes));
}

void  Osal_mmapFree (void *ptr, uint32_t size)
{
  free(ptr);
}



