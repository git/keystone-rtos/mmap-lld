/**
 *   @file  test/src/mmap_cfg_k2e_c66.c
 *
 *   @brief   
 *      This file contains the device specific configuration and initialization routines
 *      for MPAX.
 *
 *  \par
 *  ============================================================================
 *  @n   (C) Copyright 2013, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
*/

/* MPAX Types includes */
#include <stdint.h>
#include <stdlib.h>

/* MPAX includes */
#include <ti/runtime/mmap/mmap.h>

/* CSL RL includes */
#include <ti/csl/cslr_device.h>


/** @brief MPAX global device configuration. */
keystone_mmap_init_config_t mmapInitCfg = 
{
  /** Privilege ID. This must be set by application. */
  -1,

  /** file descriptor for accessing registers. (OS dependent) */
  0,

  /** XMC register base address */
  (void*)CSL_XMC_CFG_REGS,

  /** MPAX register base address (includes SES registers) */
  (void*)CSL_MSMC_CFG_REGS,

  /** CGEM register base address (includes MAR registers) */
  (void *)CSL_C66X_COREPAC_REG_BASE_ADDRESS_REGS,

  /** Number of XMC registers per core. */
  14,

  /** Start index of available XMC regs */
  2,

  /** Number of SES registers per privilege ID. */
  7,

  /** Start index of available XMC regs */
  1,

  /** Available address space. Defined by application. */
  NULL
};

