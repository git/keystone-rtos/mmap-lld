#include <stdint.h>


void Osal_mmapRegisterRead (int32_t fd, uint32_t *val, uint32_t count, uint32_t offset)
{
  uint32_t *reg = (uint32_t *)offset, i;

  for ( i = 0; i < count; i++)
  {
    val[i] = reg[i];
  }
}

void Osal_mmapRegisterWrite (int32_t fd, uint32_t *val, uint32_t count, uint32_t offset)
{
  uint32_t *reg = (uint32_t *)offset, i;

  for ( i = 0; i < count; i++)
  {
    reg[i] = val[i];
  }
}

#define OSAL_MMAP_HEAP_SIZE     0x1000

uint8_t Osal_mmap_heap[OSAL_MMAP_HEAP_SIZE];

void *Osal_mmapMalloc(uint32_t num_bytes)
{
  void *ptr = (void *)0;
  if ( num_bytes <= OSAL_MMAP_HEAP_SIZE )
    ptr = (void *)Osal_mmap_heap;

  return (ptr);
}

void Osal_mmapFree(void *ptr, uint32_t size)
{
  return;
}

