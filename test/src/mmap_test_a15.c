/**
 *   @file  mmap_test.c
 *
 *   @brief   
 *      This is the KEYSTONE_MMAP unit test code.
 *
 *  \par
 *  ============================================================================
 *  @n   (C) Copyright 2013, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
*/

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <ti/csl/cslr_msmc.h>

/* KEYSTONE_MMAP include */
#include <ti/runtime/mmap/mmap.h>


extern keystone_mmap_init_config_t mmapInitCfg;

keystone_mmap_address_range_t mmap_test_address_range[] = {
  {(uint32_t)0xA0000000, 0x60000000},
  {0,0}
};


#define MMAP_TEST_PHYS_BASE    0x820000000ULL

#define MMAP_TEST_BUFFER_SIZE   0x20000000UL    /* 512MB */
#define MMAP_TEST_WINDOW_SIZE       0x1000UL    /*   4KB */ 

void print_ses_regs(keystone_mmap_init_config_t *mmap_cfg)
{
  CSL_MsmcRegs *msmc_regs = (CSL_MsmcRegs *)mmap_cfg->msmcRegs;
  CSL_MsmcSes_mpax_per_prividRegs ses_regs;
  int32_t i;

  printf("\n\t SES REGS\n");
  pread(mmap_cfg->fd, &ses_regs, sizeof(CSL_MsmcSes_mpax_per_prividRegs), &msmc_regs->SES_MPAX_PER_PRIVID[mmap_cfg->privID]);
  for ( i = 0; i < 8; i++)
  {
    printf("\t0x%08X  |  0x%08X\n", ses_regs.SES[i].MPAXL, ses_regs.SES[i].MPAXH);
  }
}

void main(void)
{
  keystone_mmap_handle_t  h;
  keystone_mmap_init_config_t mmap_cfg;
  int32_t ret_val, i;
  uint32_t *p_buffer, *p_window, errors;

  memcpy(&mmap_cfg, &mmapInitCfg, sizeof(keystone_mmap_init_config_t));

  mmap_cfg.addr_range = mmap_test_address_range;
  mmap_cfg.privID = 8;
  mmap_cfg.fd = open("/dev/mpax", (O_RDWR | O_SYNC));

  if ( mmap_cfg.fd < 0 ) 
  {
    printf("Error opening /dev/mpax \n");
    return;
  }
  print_ses_regs(&mmap_cfg);

  h = keystone_mmap_init(&mmap_cfg);

  if ( h == NULL )
  {
    printf ("keystone_mmap_init() returned error\n");
    return;
  }
  else 
  {
    printf ("keystone_mmap_init() Suceeded\n");
  }

  print_ses_regs(&mmap_cfg);
  printf("keystone_mmap() for phys addr = 0x%016llX, size = %08X\n", (unsigned long long)MMAP_TEST_PHYS_BASE, MMAP_TEST_BUFFER_SIZE);
  p_buffer = keystone_mmap ( h, MMAP_TEST_PHYS_BASE, MMAP_TEST_BUFFER_SIZE, KEYSTONE_MMAP_PROT_READ|KEYSTONE_MMAP_PROT_WRITE, 0);

  if ( p_buffer == KEYSTONE_MMAP_MAP_FAILED)
  {
    printf("keystone_mmap() failed.\n");
    return;
  }
  else
  { 
    printf("keystone_mmap() returns 0x%08X\n", (uint32_t)p_buffer);
  }

  print_ses_regs(&mmap_cfg);

  /* Remove the buffer mapping. */
  keystone_munmap(h, p_buffer,MMAP_TEST_BUFFER_SIZE);

  print_ses_regs(&mmap_cfg);

}

