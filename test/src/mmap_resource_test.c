/**
 *   @file  mmap_test.c
 *
 *   @brief   
 *      This is a unit test for testing the allocation of resources for KEYSTONE_MMAP.
 *      This file contains no platform dependencies so it may be compiled for 
 *      DSP, ARM, x86, etc...
 *
 *      To produce the test executable, the following files must be compiled and 
 *      linked:
 *
 *        - ti/runtime/mmap/test/src/mmap_resource_test.c (this file)
 *        - ti/runtime/mmap/src/mmap_resource.c
 *
 *  \par
 *  ============================================================================
 *  @n   (C) Copyright 2013, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <ti/runtime/mmap/mmap.h>
#include <ti/runtime/mmap/include/mmap_resource.h>

/* Master copy of the newly initialized resources. */
keystone_mmap_resources_t mmap_test_resources;

/* Available address space */
uint32_t mmap_test_heap_base[2] = { 0x80000000, 0xC0000000};
uint32_t mmap_test_heap_size[2] = { 0x20000000, 0x40000000};

/* Available XMC and SES registers */
uint32_t mmap_test_xmc_regs[KEYSTONE_MMAP_MAX_NUM_MAPS] = {  4,  5,  6,  7,  8,  9, 10, 11};
uint32_t mmap_test_ses_regs[KEYSTONE_MMAP_MAX_NUM_MAPS] = {  0,  1,  2,  3,  4,  5,  6,  7};


char *default_argv[3*4] = {
  "0x820000000","0x3000000","0x03",
  "0x83FFF0000","0x1000000","0x83",
  "0x823000000","0x2000000","0x03",
  "0x826000000","0x1234567","0x03"
};


int32_t mmap_test_resource_alloc(uint32_t num_bufs, uint64_t *phys_addrs, uint32_t *lengths, uint32_t *virt_addrs, uint32_t *prot, keystone_mmap_mapping_t *mappings)
{
  keystone_mmap_resources_t loc_resources;  // Local copy of the resources.
  int32_t ret_val;

  printf("sizeof(keystone_mmap_resources_t) = %d\n", sizeof(keystone_mmap_resources_t));

  memcpy(&loc_resources, &mmap_test_resources, sizeof(keystone_mmap_resources_t));

  /* Allocate resources for the requested mapping */
  ret_val = keystone_mmap_resource_alloc( num_bufs, phys_addrs, lengths, prot, virt_addrs, &loc_resources);

  if ( ret_val != KEYSTONE_MMAP_RESOURCE_NOERR)
  {
    printf("ERROR allocating resources.\n");
  }

  /* Copy mapping table */
  memcpy(mappings, &loc_resources.mapping[0], KEYSTONE_MMAP_MAX_NUM_MAPS*sizeof(keystone_mmap_mapping_t));

#if 1
  /* Test freeing the resources. */
  ret_val = keystone_mmap_resource_free( num_bufs, virt_addrs, lengths, &loc_resources);
  if ( ret_val != KEYSTONE_MMAP_RESOURCE_NOERR)
  {
    printf("ERROR allocating resources.\n");
  }
#endif

  return (ret_val);
}



int main(int argc, char **argv)
{
  keystone_mmap_mapping_t mappings[8];
  uint64_t phys_addrs[16];
  uint32_t lengths[16], virt_addrs[16], prot[16], num_bufs = 0;
  int32_t  i, k;
  char     **argv_p = &argv[1];

  if ( argc < 3 ) 
  {
    argv_p = default_argv;
    argc   = 12;
  }
  else
  {
    argv_p = &argv[1];
    argc--;
  }

  keystone_mmap_resource_init( 8, mmap_test_xmc_regs, mmap_test_ses_regs, 2, mmap_test_heap_base, mmap_test_heap_size, &mmap_test_resources);

  printf("GDB to examine resources...\n");
  getchar();

  for ( i = 0; i < argc; i+=3 )
  {
    phys_addrs[num_bufs] = strtoull( argv_p[i], NULL, 0);
    lengths[num_bufs]      = strtoul ( argv_p[i+1], NULL, 0);

    printf("Buf %2d: 0x%016llX : 0x%08X \n", i/3, phys_addrs[num_bufs], lengths[num_bufs]);

    prot[num_bufs] = strtoul( argv_p[i+2], NULL, 0);

    num_bufs++;
  }
  printf("\n");

//for (k = 0; k < 2; k++)
{
  mmap_test_resource_alloc(num_bufs, phys_addrs, lengths, virt_addrs, prot, mappings);

  printf("\n");
  for ( i = 0; i < num_bufs; i++)
  {
    printf("Buf %2d: 0x%016llX : 0x%08X --->  0x%08X\n", i, phys_addrs[i], lengths[i], virt_addrs[i]);
  }
  printf("\n");
  for ( i = 0; i < KEYSTONE_MMAP_MAX_NUM_MAPS; i++)
  {
    if ( mappings[i].xmc_idx >= 0 && mappings[i].ses_idx >= 0 && mappings[i].segsize_power2 > 0)
    {
      int32_t j;
      uint8_t mar_base = (mappings[i].baddr >> 24);
      uint8_t num_mars = 1;

      if ( mappings[i].segsize_power2 > 24 )
        num_mars = 1 << (mappings[i].segsize_power2 - 24);

      for ( j = 0; j < num_mars; j++)
      {
        printf("MAR 0x%02X : ", mar_base + j );
        if ( (mappings[i].prot & KEYSTONE_MMAP_PROT_NOCACHE) == KEYSTONE_MMAP_PROT_NOCACHE )
          printf("NON");
        printf("CACHED\n");
      }
    }
  }

  printf("\n\nPause for debugger...\n");
  getchar();
}
  return 0;
}
