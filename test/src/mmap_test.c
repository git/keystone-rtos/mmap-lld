/**
 *   @file  mmap_test.c
 *
 *   @brief   
 *      This is the KEYSTONE_MMAP unit test code.
 *
 *  \par
 *  ============================================================================
 *  @n   (C) Copyright 2013, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  \par
*/

#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include <ti/sysbios/family/c66/Cache.h>

#include <string.h>

/* KEYSTONE_MMAP include */
#include <ti/runtime/mmap/mmap.h>

/* CSL includes */
#include <ti/csl/csl_chip.h>


#define  MMAP_TEST_EMPTY_SPACE_SIZE 0x1FFFFFFCUL
unsigned char mmap_test_empty_space[MMAP_TEST_EMPTY_SPACE_SIZE];
#pragma DATA_SECTION(mmap_test_empty_space,".empty_space");

extern keystone_mmap_init_config_t mmapInitCfg;

keystone_mmap_address_range_t mmap_test_address_range[] = {
  {(uint32_t)&mmap_test_empty_space[0], MMAP_TEST_EMPTY_SPACE_SIZE},
  {0,0}
};


#define MMAP_TEST_PHYS_BASE    0x820000000ULL

#define MMAP_TEST_BUFFER_SIZE   0x1FFFFFFFCUL    /* 512MB */
#define MMAP_TEST_WINDOW_SIZE       0x1000UL    /*   4KB */ 

void main(void)
{
  keystone_mmap_handle_t  h;
  keystone_mmap_init_config_t mmap_cfg;
  int32_t ret_val, i;
  uint32_t *p_buffer, *p_window, errors;

  memcpy(&mmap_cfg, &mmapInitCfg, sizeof(keystone_mmap_init_config_t));

  mmap_cfg.privID = DNUM;
  mmap_cfg.addr_range = mmap_test_address_range;

  h = keystone_mmap_init(&mmap_cfg);

  if ( h == NULL )
  {
    System_printf ("keystone_mmap_init() returned error\n");
    return;
  }
  else 
  {
    System_printf ("keystone_mmap_init() Suceeded\n");
  }

  System_printf("keystone_mmap() for phys addr = 0x%016llX, size = %08X\n", (unsigned long long)MMAP_TEST_PHYS_BASE, MMAP_TEST_BUFFER_SIZE);
  p_buffer = keystone_mmap ( h, MMAP_TEST_PHYS_BASE, MMAP_TEST_BUFFER_SIZE, KEYSTONE_MMAP_PROT_READ|KEYSTONE_MMAP_PROT_WRITE, 0);

  if ( p_buffer == KEYSTONE_MMAP_MAP_FAILED)
  {
    System_printf("keystone_mmap() failed.\n");
    return;
  }
  else
  { 
    System_printf("keystone_mmap() returns 0x%08X\n", (uint32_t)p_buffer);
  }

  /* Initialize the large buffer */
  for ( i = 0; i < (MMAP_TEST_BUFFER_SIZE/sizeof(uint32_t)); i++)
  {
    p_buffer[i] = i;
  }
  /* Write back cache to the physical memory so that we can use another mapping to access data. */
  Cache_wb(p_buffer, MMAP_TEST_BUFFER_SIZE, Cache_Type_ALL, TRUE);


  /* Use KEYSTONE_MMAP to map a moving window */
  errors = 0;
  for ( i = 0; i < (MMAP_TEST_BUFFER_SIZE/MMAP_TEST_WINDOW_SIZE); i++)
  {
    uint8_t *c_p_buffer = (uint8_t *)p_buffer;

    /* Map a smaller window into the larger physical memory. */
    p_window = keystone_mmap( h, (MMAP_TEST_PHYS_BASE + i*MMAP_TEST_WINDOW_SIZE), MMAP_TEST_WINDOW_SIZE, KEYSTONE_MMAP_PROT_READ, 0);
    if ( p_window == KEYSTONE_MMAP_MAP_FAILED)
    {
      System_printf("keystone_mmap() failed for phys addr = 0x%016llX, size = %08X\n", (unsigned long long)(MMAP_TEST_PHYS_BASE + i*MMAP_TEST_WINDOW_SIZE), MMAP_TEST_WINDOW_SIZE);
      return;
    }

    /* Invalidate so we can read valid data. */
    Cache_inv(p_window,MMAP_TEST_WINDOW_SIZE,Cache_Type_ALL, TRUE);

    /* Verify that the data is correct. */
    if ( memcmp( &(((uint8_t *)p_buffer)[i*MMAP_TEST_WINDOW_SIZE]), p_window, MMAP_TEST_WINDOW_SIZE) != 0)
      errors++;

    /* Remove window mapping. */
    keystone_munmap( h, p_window, MMAP_TEST_WINDOW_SIZE);
  }

  System_printf("There were %d errors\n", errors);

  /* Remove the buffer mapping. */
  keystone_munmap(h, p_buffer,MMAP_TEST_BUFFER_SIZE);

}

