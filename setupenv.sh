#!/bin/bash

export TI_INSTALL_DIR=~/dev/ti

export XDC_INSTALL_PATH=$TI_INSTALL_DIR/xdctools_3_25_03_72
export C6X_GEN_INSTALL_PATH=$TI_INSTALL_DIR/ccsv5/tools/compiler/c6000_7.4.4
export MMAP_INSTALL_PATH=$TI_INSTALL_DIR/mpax_install
export CG_XML_BIN_INSTALL_PATH=$TI_INSTALL_DIR/cg_xml/bin
export PDK_INSTALL_PATH=$TI_INSTALL_DIR/pdk_keystone2_3_00_03_15/packages

export SOC=K2H

export PARTNO=TCI6614

export CGTOOLS=$C6X_GEN_INSTALL_PATH

export PATH=$PATH:$XDC_INSTALL_PATH:$XDC_INSTALL_PATH/packages/xdc/services/io/release:$C6X_GEN_INSTALL_PATH/bin:$CG_XML_BIN_INSTALL_PATH

export XDCCGROOT=$C6X_GEN_INSTALL_PATH

export XDCPATH=../../..:$XDC_INSTALL_PATH/packages:$C6X_GEN_INSTALL_PATH/include:$PDK_INSTALL_PATH

export CROSS_TOOL_INSTALL_PATH=$TI_INSTALL_DIR/linaro/gcc-linaro-arm-linux-gnueabihf-4.7-2013.03-20130313_linux/bin
export CROSS_TOOL_PRFX=arm-linux-gnueabihf-

