/**
 *   @file  mmap_resource.h
 *
 *   @brief
 *      Private header file for the KEYSTONE_MMAP component which relevent to the 
 *      allocation of resources such as logical addresses and MPAX registers.
 *
 *  \par
 *  NOTE:
 *      (C) Copyright 2013 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef __KEYSTONE_MMAP_RESOURCE_H
#define __KEYSTONE_MMAP_RESOURCE_H
#include <stdint.h>


/**
 *  @brief   Maximum number of individual mappings supported by the MPAX 
 *           component. This number was chosen because of the HW limit of K2H.
 */
#define KEYSTONE_MMAP_MAX_NUM_MAPS         8

/**
 *  @brief   Maximum number of heap elements. 
 *
 *  @details Because the HW only supports power 
 *           of 2 sizes and alignments for mapping, the addressi space heap is 
 *           organized into a binary tree. This allows for efficient 
 *           defragmentation when mappings are freed. 
 *
 *           This number was chosen with respect to the maximum number of 
 *           mappings allowed by the HW. The maximum theoretical size of the 
 *           available address space is 2^32 bytes, and the minimum size that 
 *           can be mapped is 2^12 bytes. This would allow a maximum of 2^20 
 *           mappings, and require 2^21 heap elements. However, the HW supports 
 *           a maximum of 8 mappings.
 */
#define KEYSTONE_MMAP_MAX_NUM_HEAP_ELEMS   80


/**
 *  @brief   Error codes returned by the mmap_resource APIs
 */
enum {
  KEYSTONE_MMAP_RESOURCE_NOERR,
  KEYSTONE_MMAP_RESOURCE_INIT_ERROR,
  KEYSTONE_MMAP_RESOURCE_FREE_ERROR,
  KEYSTONE_MMAP_RESOURCE_ALLOC_ERROR
};


/**
 *  @brief   Cache options for mapped memory.
 */
enum {
  /** 
   *  @brief   Map as cached memory.
   */
  KEYSTONE_MMAP_CACHED,

  /**
   *  @brief   Map as noncached memory.
   */
  KEYSTONE_MMAP_NONCACHED
};



/**
 *  @brief   This structure provides the mapping information.
 */
typedef struct {
  /**
   *  @brief   "Replacement Address": The 36-bit physical address of the memory 
   *           to be mapped.
   */
  uint64_t raddr;

  /**
   *  @brief   "Base Address": The 32-bit virtual address at which the physical 
   *           memory may be accessed.
   */
  uint32_t baddr;

  /**
   *  @brief   XMC register index to be used in the mapping.
   */
  int8_t   xmc_idx;

  /**
   *  @brief   SES register index to be used in the mapping.
   */
  int8_t   ses_idx;

  /**
   *  @brief   Log base 2 of the size of the memory to be mapped.
   */
  int8_t   segsize_power2;

  /**
   *  @brief   RWX and cache configuration to be applied to the memory. 
   */
  uint8_t  prot;

} keystone_mmap_mapping_t;


/**
 *  @brief   This strucutre is the basic element of the address space heap.
 *
 *  @details These elements are used to construct a binary tree representing the
 *           fragmentation of the address space into power of 2 sizes. They are 
 *           statically allocated in the keystone_mmap_resources_t structure in the heap 
 *           elemnt bank. 
 */
typedef struct keystone_mmap_heap_element_s {

  /**
   *  @brief   Base address of the address space segment. Must be aligned to the 
   *           length.
   */
  uint32_t        base_addr;

  /**
   *  @brief   Length of the address space segment. Must be a power of 2.
   */
  uint32_t        length;

  /**
   *  @brief   Flag to indicate if the element is being used within the heap 
   *           structure.
   */
  uint8_t         in_use;

  /** 
   *  @brief   Number of address space segments allocated for mapping within 
   *           this segment.
   */
  uint8_t         allocated;

  /**
   *  @brief   Indicates whether this segment is cached of noncached.
   *
   *  @details Only applicable for segments of size KEYSTONE_MMAP_HEAP_ALIGN or smaller, 
   *           and only if allocated is greater than 0.
   */
  uint8_t         prot;

  /**
   *  @brief   The sub elements are the current element split into 2 equal sized 
   *           segments. They are used to fragment the address space into power 
   *           of 2 sized segments.
   */
  uint32_t        sub_elem[2];

} keystone_mmap_heap_element_t;


/**
 *  @brief   This strucutre maintains the keystone_mmap resources for a specific context. 
 *
 *  @details This strucutre maintains the mapping table and the address space 
 *           heaps.
 */
typedef struct keystone_mmap_resources_s {

  /** 
   *  @brief   Mapping table.
   */
  keystone_mmap_mapping_t       mapping[KEYSTONE_MMAP_MAX_NUM_MAPS];

  /**
   *  @brief   Heap element bank.
   *
   *  @details The first num_heaps elements are the root nodes of the address 
   *           space heaps.
   */
  keystone_mmap_heap_element_t  heap[KEYSTONE_MMAP_MAX_NUM_HEAP_ELEMS];

  /**
   *  @brief   Number of address space heaps.
   */
  uint32_t             num_heaps;

} keystone_mmap_resources_t;


/**
 *  @b  Description
 *
 *  @n
 *      This function initializes the data structures which manage the resources
 *      used for mapping. These resources are the address space heaps and MPAX 
 *      registers used for mapping.
 *
 *  @param[in]  num_regs   Number of XMC and SES register pairs available for 
 *                         mapping.
 *
 *  @param[in]  xmc_regs   Array of XMC register indexes which are available for 
 *                         mapping. XMC registers allow the DSP core to access 
 *                         the mapped memory.
 *
 *  @param[in]  ses_regs   Array of SES register indexes which are available for
 *                         mapping. SES registers allows EDMA to access the 
 *                         mapped memory.
 *
 *  @param[in]  num_heaps  Number of contiguous address space ranges which are 
 *                         available for mapping.
 *
 *  @param[in]  heap_base  Array of base addresses of each contiguous address 
 *                         space range.
 *
 *  @param[in]  heap_size  Array of sizes of each contiguous address space range.
 *
 *  @param[out] resources  KEYSTONE_MMAP resource strucutre which manages the resources 
 *                         used for mapping.
 *
 *  @retval     KEYSTONE_MMAP_RESOURCE_NOERR on success. Other values correspond to an 
 *              error condition. (@sa )
 */
int32_t keystone_mmap_resource_init
(
  uint32_t            num_regs, 
  uint32_t           *xmc_regs, 
  uint32_t           *ses_regs, 
  uint32_t            num_heaps, 
  uint32_t           *heap_base, 
  uint32_t           *heap_size, 
  keystone_mmap_resources_t   *resources
);


/**
 *  @b  Description
 *
 *  @n
 *      This functions takes physical buffers and lengths and allocates the 
 *      resources required to map all of the buffer. If the allocation is 
 *      successful, the array of addressable virtual address is populated along 
 *      with the physical mapping configuration.
 *
 *  @param[in]     num_bufs     Number of buffers for which to allocate mapping 
 *                              resources.
 *
 *  @param[in]     phys_addrs   Array of physical addresses of the base of each 
 *                              buffer.
 *
 *  @param[in]     lengths      Array of lengths for each buffer.
 *
 *  @param[in]     protections  Array containing the read/write/execute 
 *                              permissions along with cacheability.
 *
 *  @param[out]    virt_addrs   An array which, upon succesful resource 
 *                              allocation, will be populated with the virtual 
 *                              addresses corresponding to the physical buffers.
 * 
 *  @param[in,out] resources    Resource structure which manages the mapping 
 *                              resources. This structure contains a 
 *                              keystone_mmap_mapping_t structure which is the table of 
 *                              mapping information which should be passed to 
 *                              keystone_mmap_map() to perform the actual mapping.
 *
 *  @retval        KEYSTONE_MMAP_RESOURCE_NOERR on success. Other values correspond to an 
 *                 error condition. (@sa )
 */
int32_t keystone_mmap_resource_alloc
( 
  uint32_t            num_bufs, 
  uint64_t           *phys_addrs, 
  uint32_t           *lengths, 
  uint32_t           *protections, 
  uint32_t           *virt_addrs, 
  keystone_mmap_resources_t   *resources
);


/**
 *  @b  Description
 *
 *  @n  
 *      This function frees resources associated to mapped physical buffers, The 
 *      virtual addresses and lengths are supplied to this function and the 
 *      corresponding mapping entries are removed.
 *
 *  @param[in]     num_bufs   number of buffers which are to be unmapped.
 *
 *  @param[in]     virt_addrs Array of virtual address of the buffers which are 
 *                            to be unmapped.
 *
 *  @param[in]     lengths    Array of lengths of the buffers which are to be 
 *                            unmapped.
 *
 *  @param[in,out] resources  Resource structure managing the resources used for 
 *                            mapping.
 *
 *  @retval        KEYSTONE_MMAP_RESOURCE_NOERR on success. Other values correspond to an 
 *                 error condition. (@sa )
 */
int32_t keystone_mmap_resource_free
( 
  uint32_t            num_bufs, 
  uint32_t           *virt_addrs, 
  uint32_t           *lengths, 
  keystone_mmap_resources_t   *resources
);



#endif /* __KEYSTONE_MMAP_RESOURCE_H */
/* nothing past this point */

