/**
 *   @file  mmap_map.c
 *
 *   @brief
 *      Source file for modifying the MPAX registers for performing the 
 *      actual mapping.
 *
 *  \par
 *  NOTE:
 *      (C) Copyright 2013 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdint.h>
#include <string.h>

#include <ti/csl/cslr.h>
#include <ti/csl/csl_chip.h>
#include <ti/csl/cslr_msmc.h>
#include <ti/csl/cslr_xmc.h>
#include <ti/csl/cslr_cgem.h>

#include <ti/runtime/mmap/mmap.h>
#include <ti/runtime/mmap/include/mmap_resource.h>
#include <ti/runtime/mmap/include/mmap_map.h>
#include <ti/runtime/mmap/mmap_osal.h>


/**
 *  @b  Description
 *
 *  @n
 *      This functions takes the mapping table and configures the hardware so 
 *      that the mappings are realized.
 *
 *  @param[in]    mapping       Table of mappings.
 *
 *  @param[in]    init_config   Structure which contains register base addresses
 *
 *  @retval       0 on success.
 */
int32_t keystone_mmap_do_map(keystone_mmap_mapping_t *mapping, keystone_mmap_init_config_t *init_config)
{
  CSL_XmcRegs   *xmc   = (CSL_XmcRegs *)init_config->xmcRegs;
  CSL_MsmcRegs  *msmc  = (CSL_MsmcRegs *)init_config->msmcRegs;
  CSL_CgemRegs  *cgem  = (CSL_CgemRegs *)init_config->cgemRegs;
  uint32_t      xmc_mpax[2];
  uint32_t      ses_mpax[2];
  uint32_t      base_addr;
  int16_t       segsize_power2;
  uint32_t      replace_addr;
  uint32_t      perms;
  int32_t       privID = init_config->privID;
  int32_t       reg_fd = init_config->fd;
  uint8_t       mar_base, mar_cnt;
  uint32_t      mar;
  int32_t       i, j;

  if ( !mmap_osalRegisterRead || 
       !mmap_osalRegisterWrite )
  {
    return -1;
  }

  for ( i = 0; i < KEYSTONE_MMAP_MAX_NUM_MAPS; i++)
  {
    keystone_mmap_mapping_t *map = &mapping[i];

    if ( map->xmc_idx >= 0 && map->ses_idx >= 0 )
    {
      memset( xmc_mpax, 0, 2*sizeof(uint32_t));
      memset( ses_mpax, 0, 2*sizeof(uint32_t));
      
      replace_addr   = (uint32_t)(map->raddr >> 12);
      base_addr      = (uint32_t)(map->baddr >> 12);
      segsize_power2 = (int16_t)  map->segsize_power2;
      perms          = (uint32_t) map->prot;

      if ( segsize_power2 ) {
        segsize_power2--;
      } else {
        segsize_power2 = 0;
      }

      if ( perms & KEYSTONE_MMAP_PROT_READ )
      {
        CSL_FINS ( xmc_mpax[0], XMC_XMPAXL_UR, 1);
        CSL_FINS ( xmc_mpax[0], XMC_XMPAXL_SR, 1);
        CSL_FINS ( ses_mpax[0], MSMC_SES_MPAXL_0_UR, 1);
        CSL_FINS ( ses_mpax[0], MSMC_SES_MPAXL_0_SR, 1);
      }
      if ( perms & KEYSTONE_MMAP_PROT_WRITE )
      {
        CSL_FINS ( xmc_mpax[0], XMC_XMPAXL_UW, 1);
        CSL_FINS ( xmc_mpax[0], XMC_XMPAXL_SW, 1);
        CSL_FINS ( ses_mpax[0], MSMC_SES_MPAXL_0_UW, 1);
        CSL_FINS ( ses_mpax[0], MSMC_SES_MPAXL_0_SW, 1);
      }
      if ( perms & KEYSTONE_MMAP_PROT_EXEC )
      {
        CSL_FINS ( xmc_mpax[0], XMC_XMPAXL_UX, 1);
        CSL_FINS ( xmc_mpax[0], XMC_XMPAXL_SX, 1);
        CSL_FINS ( ses_mpax[0], MSMC_SES_MPAXL_0_UX, 1);
        CSL_FINS ( ses_mpax[0], MSMC_SES_MPAXL_0_SX, 1);
      }

      CSL_FINS ( xmc_mpax[0], XMC_XMPAXL_RADDR, replace_addr);
      CSL_FINS ( ses_mpax[0], MSMC_SES_MPAXL_0_RADDR, replace_addr);

      CSL_FINS ( xmc_mpax[1], XMC_XMPAXH_SEGSZ, segsize_power2);
      CSL_FINS ( ses_mpax[1], MSMC_SES_MPAXH_0_SEGSZ, segsize_power2);

      CSL_FINS ( xmc_mpax[1], XMC_XMPAXH_BADDR, base_addr);
      CSL_FINS ( ses_mpax[1], MSMC_SES_MPAXH_0_BADDR, base_addr);

      /* Set XMC and SES registers */
      if ( xmc != NULL )
      {
        mmap_osalRegisterWrite(reg_fd, xmc_mpax, 2, (uint32_t)&xmc->XMPAX[map->xmc_idx]);
      }

      if ( msmc != NULL )
      {
        mmap_osalRegisterWrite(reg_fd, ses_mpax, 2, (uint32_t)&msmc->SES_MPAX_PER_PRIVID[privID].SES[map->ses_idx]);
      }

      /* Configure cacheability */
      if ( cgem != NULL )
      {
        if ( segsize_power2 > 0 )
        {
          mar_base = map->baddr >> 24;
          mar_cnt = 1;

          /* Check if mapping extends across multiple MAR regions. */
          if ( map->segsize_power2 > 24 )
            mar_cnt = 1 << (map->segsize_power2 - 24);

          for ( j = 0; j < mar_cnt; j++) 
          {
            mmap_osalRegisterRead(reg_fd, &mar, 1, (uint32_t)&cgem->MAR[mar_base + j]);

            if ( (map->prot & KEYSTONE_MMAP_PROT_NOCACHE) == KEYSTONE_MMAP_PROT_NOCACHE )
            {
              CSL_FINS(mar, CGEM_MAR0_PC, 0);
            }
            else
            {
              CSL_FINS(mar, CGEM_MAR0_PC, 1);
            }

            mmap_osalRegisterWrite(reg_fd, &mar, 1, (uint32_t)&cgem->MAR[mar_base + j]);
          }
        }
      }
    }
  }
  return 0;
} /* keystone_mmap_do_map() */

/* nothing past this point */

