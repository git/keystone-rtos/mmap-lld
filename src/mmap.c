/**
 *   @file  mmap.c
 *
 *   @brief
 *      Source file for the KEYSTONE_MMAP component. This file implements the public APIs 
 *      of the KEYSTONE_MMAP component.
 *
 *  \par
 *  NOTE:
 *      (C) Copyright 2013 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdint.h>
#include <string.h>
#include <stdio.h>

#include <ti/csl/cslr.h>
#include <ti/csl/cslr_msmc.h>
#include <ti/csl/cslr_xmc.h>


#include <ti/runtime/mmap/mmap.h>
#include <ti/runtime/mmap/include/mmap_resource.h>
#include <ti/runtime/mmap/include/mmap_map.h>
#include <ti/runtime/mmap/mmap_osal.h>

#define KEYSTONE_MMAP_MAX_NUM_HEAPS            32 


//#define KEYSTONE_MMAP_PROFILE
#ifdef KEYSTONE_MMAP_PROFILE
extern cregister volatile unsigned int TSCL;
uint32_t TSCL_Begin, TSCL_End;
uint32_t keystone_mmap_prof_resource_alloc_cycles;
uint32_t keystone_mmap_prof_resource_alloc_count;
uint32_t keystone_mmap_prof_resource_free_cycles;
uint32_t keystone_mmap_prof_resource_free_count;
uint32_t keystone_mmap_prof_map_cycles;
uint32_t keystone_mmap_prof_map_count;
#endif

typedef struct {
  keystone_mmap_init_config_t cfg;
  keystone_mmap_resources_t   res;
} keystone_mmap_inst_t;

/*
 *  Function: keystone_mmap_init()
 */
keystone_mmap_handle_t keystone_mmap_init( keystone_mmap_init_config_t *init_config)
{
  keystone_mmap_inst_t *inst = NULL;
  uint32_t num_heaps = 0, heap_base[KEYSTONE_MMAP_MAX_NUM_HEAPS], heap_size[KEYSTONE_MMAP_MAX_NUM_HEAPS];
  uint32_t num_regs = 0, xmc_idx = 0, ses_idx = 0, xmc_regs[KEYSTONE_MMAP_MAX_NUM_MAPS], ses_regs[KEYSTONE_MMAP_MAX_NUM_MAPS];
  int32_t  ret_val;

#ifdef KEYSTONE_MMAP_PROFILE
  keystone_mmap_prof_resource_alloc_cycles=0;
  keystone_mmap_prof_resource_alloc_count=0;
  keystone_mmap_prof_resource_free_cycles=0;
  keystone_mmap_prof_resource_free_count=0;
  keystone_mmap_prof_map_cycles=0;
  keystone_mmap_prof_map_count=0;
#endif  

  if ( !mmap_osalMalloc        ||
       !mmap_osalFree          ||
       !mmap_osalRegisterRead  ||
       !mmap_osalRegisterWrite )
  {
    return NULL;
  }

  while ( (num_heaps < KEYSTONE_MMAP_MAX_NUM_HEAPS) && (init_config->addr_range[num_heaps].base != 0) )
  {
    heap_base[num_heaps] = init_config->addr_range[num_heaps].base;
    heap_size[num_heaps] = init_config->addr_range[num_heaps].size;
    num_heaps++;
  }

  inst = mmap_osalMalloc(sizeof(keystone_mmap_inst_t));
  if ( inst == NULL )
  {
    return ( 0 );
  }

  memcpy(&inst->cfg, init_config, sizeof(keystone_mmap_init_config_t));

  /* 
   * Initialize available MPAX regs.
   */

  /* If XMC not used, set available regs to max to keep same logic for when they 
   * are used, even though mappings will not be attempted.
   */
  if ( (inst->cfg.xmcRegs == NULL) || (inst->cfg.xmcXmpaxRegCount > KEYSTONE_MMAP_MAX_NUM_MAPS) )
  {
    inst->cfg.xmcXmpaxRegCount = KEYSTONE_MMAP_MAX_NUM_MAPS;
  }

  for ( xmc_idx = 0; (int)xmc_idx < inst->cfg.xmcXmpaxRegCount; xmc_idx++)
  {
    xmc_regs[xmc_idx] = xmc_idx + inst->cfg.xmcXmpaxRegStart;
  }

  /* If SES not used, set available regs to max to keep same logic for when they 
   * are used, even though mappings will not be attempted.
   */
  if ( (inst->cfg.msmcRegs == NULL) || (inst->cfg.sesMpaxRegsPerPrividCount > KEYSTONE_MMAP_MAX_NUM_MAPS) )
  {
    inst->cfg.sesMpaxRegsPerPrividCount = KEYSTONE_MMAP_MAX_NUM_MAPS;
  }

  for ( ses_idx = 0; (int)ses_idx < inst->cfg.sesMpaxRegsPerPrividCount; ses_idx++)
  {
    ses_regs[ses_idx] = ses_idx + inst->cfg.sesMpaxRegsPerPrividStart;
  }

  num_regs = (xmc_idx < ses_idx) ? xmc_idx : ses_idx;

  ret_val = keystone_mmap_resource_init
            ( 
              num_regs,
              xmc_regs, 
              ses_regs,
              num_heaps, 
              heap_base,
              heap_size,
              &inst->res 
            );

  if ( ret_val != KEYSTONE_MMAP_RESOURCE_NOERR )
  {
    mmap_osalFree( inst, sizeof(keystone_mmap_inst_t));
    inst = NULL;
  }

  return ((keystone_mmap_handle_t)inst);
} /* keystone_mmap_init() */


/*
 * Function: keystone_mmap_free() 
 */
void keystone_mmap_free(keystone_mmap_handle_t *h)
{
  keystone_mmap_inst_t *inst = (keystone_mmap_inst_t *)h;
  int32_t i;

  for ( i = 0 ; i < KEYSTONE_MMAP_MAX_NUM_MAPS; i++)
  {
    inst->res.mapping[i].raddr           = 0;
    inst->res.mapping[i].baddr           = 0;
    inst->res.mapping[i].segsize_power2  = 0;
    inst->res.mapping[i].prot            = KEYSTONE_MMAP_PROT_NONE;
  }
  keystone_mmap_do_map( inst->res.mapping, &inst->cfg);

  mmap_osalFree( inst, sizeof(keystone_mmap_inst_t));
} /* keystone_mmap_free() */


/*
 *  Function: keystone_mmap()
 */
//void *mmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset)
void *keystone_mmap(keystone_mmap_handle_t h, uint64_t phys_addr, size_t length, uint32_t prot, uint32_t flags)
{
  keystone_mmap_inst_t *inst = (keystone_mmap_inst_t *)h;
  uint32_t len, virt_addr, protections;
  int32_t  ret_val;

  len          = (uint32_t)length;
  protections  = (uint32_t)prot;

#ifdef KEYSTONE_MMAP_PROFILE
  TSCL_Begin = TSCL;
#endif
  ret_val = keystone_mmap_resource_alloc
            (
              1,
              &phys_addr,
              &len,
              &protections,
              &virt_addr,
              &inst->res
            );
#ifdef KEYSTONE_MMAP_PROFILE
  TSCL_End = TSCL;
  keystone_mmap_prof_resource_alloc_cycles += (TSCL_End - TSCL_Begin);
  keystone_mmap_prof_resource_alloc_count++;
  printf("KEYSTONE_MMAP Resource Alloc Cycles: %9d | %9d\n", (TSCL_End - TSCL_Begin), (keystone_mmap_prof_resource_alloc_cycles/keystone_mmap_prof_resource_alloc_count));
#endif
 
  if ( ret_val != KEYSTONE_MMAP_RESOURCE_NOERR )
    return KEYSTONE_MMAP_MAP_FAILED;

#ifdef KEYSTONE_MMAP_PROFILE
  TSCL_Begin = TSCL;
#endif
  ret_val = keystone_mmap_do_map( inst->res.mapping, &inst->cfg);
#ifdef KEYSTONE_MMAP_PROFILE
  TSCL_End = TSCL;
  keystone_mmap_prof_map_cycles += (TSCL_End - TSCL_Begin);
  keystone_mmap_prof_map_count++;
  printf("KEYSTONE_MMAP Map Cycles: %9d | %9d\n", (TSCL_End - TSCL_Begin), (keystone_mmap_prof_map_cycles/keystone_mmap_prof_map_count));
#endif
 
  if ( ret_val != 0 )
  {
    keystone_mmap_resource_free( 1, &virt_addr, &len, &inst->res);
    return KEYSTONE_MMAP_MAP_FAILED;
  }

  return ((void *)virt_addr);
} /* keystone_mmap() */


/*
 *  Function: munmap()
 */
//int munmap(int fd, void *addr, size_t length)
int32_t keystone_munmap(keystone_mmap_handle_t h, void *ptr, size_t length)
{
  keystone_mmap_inst_t *inst  = (keystone_mmap_inst_t *)h;
  uint32_t virt_addr = (uint32_t)ptr;
  uint32_t len       = (uint32_t)length;
  int32_t  ret_val;

#ifdef KEYSTONE_MMAP_PROFILE
  TSCL_Begin = TSCL;
#endif
  ret_val = keystone_mmap_resource_free( 1, &virt_addr, &len, &inst->res);
#ifdef KEYSTONE_MMAP_PROFILE
  TSCL_End = TSCL;
  keystone_mmap_prof_resource_free_cycles += (TSCL_End - TSCL_Begin);
  keystone_mmap_prof_resource_free_count++;
  printf("KEYSTONE_MMAP Resource Free Cycles: %9d | %9d\n", (TSCL_End - TSCL_Begin), (keystone_mmap_prof_resource_free_cycles/keystone_mmap_prof_resource_free_count));
#endif
 
  if ( ret_val != KEYSTONE_MMAP_RESOURCE_NOERR )
    return (-1);

#ifdef KEYSTONE_MMAP_PROFILE
  TSCL_Begin = TSCL;
#endif
  ret_val = keystone_mmap_do_map( inst->res.mapping, &inst->cfg);
#ifdef KEYSTONE_MMAP_PROFILE
  TSCL_End = TSCL;
  keystone_mmap_prof_map_cycles += (TSCL_End - TSCL_Begin);
  keystone_mmap_prof_map_count++;
  printf("KEYSTONE_MMAP Map Cycles: %9d | %9d\n", (TSCL_End - TSCL_Begin), (keystone_mmap_prof_map_cycles/keystone_mmap_prof_map_count));
#endif
 
  if ( ret_val != KEYSTONE_MMAP_RESOURCE_NOERR)
    return (-1);

  return 0;
} /* munmap() */

/*nothing past this point */

