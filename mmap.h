/**
 *   @file  mmap.h
 *
 *   @brief
 *      Header file for the KEYSTONE MMAP component. The file exposes the data structures
 *      and exported API which are available for use by applications.
 *
 *  \par
 *  NOTE:
 *      (C) Copyright 2013 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

 
 
/** @defgroup KEYSTONE_MMAP_API KEYSTONE MMAP
 */
#ifndef __KEYSTONE_MMAP_H_
#define __KEYSTONE_MMAP_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
* Shut off: remark #880-D: parameter "descType" was never referenced
*
* This is better than removing the argument since removal would break
* backwards compatibility
*/
#ifdef _TMS320C6X
#pragma diag_suppress 880
#pragma diag_suppress 681
#elif defined(__GNUC__)
/* Same for GCC:
* warning: unused parameter descType [-Wunused-parameter]
*/
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#endif
#include <stdint.h>

/**
@defgroup KEYSTONE_MMAP_SYMBOL  KEYSTONE MMAP Defined Symbols
@ingroup KEYSTONE_MMAP_API
*/
/**
@defgroup KEYSTONE_MMAP_ERROR_CODE  KEYSTONE MMAP Error codes
@ingroup KEYSTONE_MMAP_API
*/        
/**
@defgroup KEYSTONE_MMAP_FUNCTION  KEYSTONE MMAP Functions
@ingroup KEYSTONE_MMAP_API
*/
/**
@defgroup KEYSTONE_MMAP_DATA_STRUCTURE  KEYSTONE MMAP Data Structures
@ingroup KEYSTONE_MMAP_API
*/
/**
@defgroup PKTLIB_OSAL_API  KEYSTONE MMAP OS Abstraction Layer
@ingroup KEYSTONE_MMAP_API
*/


/** @addtogroup KEYSTONE_MMAP_SYMBOL
 *  @{
 */


/**
 *  @brief   This is the alignment requirement for for the 32-bit address ranges 
 *           which MMAP will use for mapping physical memory.
 */
#define KEYSTONE_MMAP_HEAP_ALIGN        0x1000000

/** 
 *  @}
 */


/** @addtogroup KEYSTONE_MMAP_DATA_STRUCTURE
 *  @{
 */
/**
 *  @brief    This structure is used to initialize mmap with available logical 
 *            address ranges. 
 *
 *  @details  Current implementation requires that the size is a power of 2 and 
 *            that the base address is aligned to this size.
 */
typedef struct 
{
  /**
   *  @brief    Base address of an available address range.
   *
   *  @details  Must be aligned to a KEYSTONE_MMAP_HEAP_ALIGN
   */
  unsigned int base;

  /**
   *  @brief    Size of the available address range.
   *
   *  @details  Must be a multiple of KEYSTONE_MMAP_HEAP_ALIGN
   */
  unsigned int size;

} keystone_mmap_address_range_t;

/**
 *  @brief    This structure is used to initialize the Keystone MMAP component 
 *            with device specific information.
 */
typedef struct 
{
  /**
   *  @brief    Privilege ID used for indexing SES registers.
   */
  int             privID;

  /** 
   *  @brief    File descriptor used for accessing device registers.
   */
  int             fd;

  /**
   *  @brief    Pointer to the base of the XMC registers.
   */
  void           *xmcRegs;

  /**
   *  @brief    Pointer to the base of the MSMC registers.
   */
  void           *msmcRegs;

  /** 
   *  @brief    Pointer to the base of the CGEM registers for accessing the MAR 
   *            registers for cache configuration.
   */
  void           *cgemRegs;

  /**
   *  @brief    Number of XMC registers available.
   */
  int             xmcXmpaxRegCount;

  /**
   *  @brief    Start index of available XMC registers.
   */
  int             xmcXmpaxRegStart;

  /**
   *  @brief    Number of SES registers available.
   */
  int             sesMpaxRegsPerPrividCount;

  /**
   *  @brief    Start index of available SES registers.
   */
  int             sesMpaxRegsPerPrividStart;

  /**
   *  @brief   Pointer to an array of address ranges which are available to be 
   *           mapped to physical memory. The final entry in the array will not 
   *           be used and must have the 'base' field set to (unsigned int)0.
   */
  keystone_mmap_address_range_t *addr_range;

} keystone_mmap_init_config_t;


typedef void* keystone_mmap_handle_t;

/**
 *  @}
 */


/** @addtogroup KEYSTONE_MMAP_FUNCTION
 *  @{
 */
/**
 *  @b Description
 *
 *  @n
 *      This function initializes an instance of Keystone MMAP. The resources an 
 *      instance has available are provided within the keystone_mmap_init_config_t 
 *      structure.
 *
 *  @param[in]  init_config   Initialization structure that contains device 
 *                            specific information.
 *
 *  @retval     mmap handle. Value will be NULL on error.
 */
keystone_mmap_handle_t keystone_mmap_init ( keystone_mmap_init_config_t *init_config);

/**
 *  @b Description
 *
 *  @n
 *      This function reset the mappings controlled by the keystone_mmap_handle_t h 
 *      and frees dynamic memory allocated for the handle.
 *
 *  @param[in]  h               Handle to an instance of MMAP
 */
void keystone_mmap_free ( keystone_mmap_handle_t *h);

/**
 *  @}
 */



/**
 *  @addtogroup KEYSTONE_MMAP_SYMBOL
 *  @{
 */

/** @defgroup KEYSTONE_MMAP_SYMBOL_PROT Protections
 *
 *  @brief    Protections are chosen from these bits, or-ed together.
 *
 *  @{
 */

/**
 *  @brief  Memory will not be cached.  
 *
 *  @note Not applicable for DMA only mappings.
 */
#define KEYSTONE_MMAP_PROT_NOCACHE    0x80    /* pages are not cached */



/**
 *  @brief  No permissions.
 */
#define	KEYSTONE_MMAP_PROT_NONE	      0x00    /* no permissions */

/**
 *  @brief  Memory can be read.
 */
#define	KEYSTONE_MMAP_PROT_READ       0x01   /* pages can be read */

/**
 *  @brief  Memory can be written.
 */
#define	KEYSTONE_MMAP_PROT_WRITE      0x02   /* pages can be written */

/**
 *  @brief  Memory can be executed.
 */
#define	KEYSTONE_MMAP_PROT_EXEC       0x04   /* pages can be executed */

/**
 *  @}
 */

/** @defgroup KEYSTONE_MMAP_SYMBOL_FLAG Flags
 *
 *  @brief  Flag used to configure mapping.
 *
 *  @{
 */
/*
 * Other flags
 */
/**
 *  @}
 */
/**
 &  @}
 */


/** @addtogroup KEYSTONE_MMAP_ERROR_CODE
 *  @{
 */
/**
 *  @brief  Error return from keystone_mmap()
 */
#define KEYSTONE_MMAP_MAP_FAILED	((void *)-1)

/**
 *  @}
 */

/** @addtogroup KEYSTONE_MMAP_FUNCTION
 * @{
 */
/** 
 *  @brief  Map physical memroy to logical address.
 */
void *	keystone_mmap(keystone_mmap_handle_t h, uint64_t phys_addr, size_t length, uint32_t prot, uint32_t flags);


/**
 *  @brief  Unmap physical memory which was mapped to logical address.
 */
int32_t keystone_munmap(keystone_mmap_handle_t h, void *ptr, size_t length);
/**
 *  @}
*/
#endif /* !__KEYSTONE_MMAP_H_ */
