#ifndef __KEYSTONE_MMAP_OSAL_H__
#define __KEYSTONE_MMAP_OSAL_H__
#include <stdint.h>

extern void  Osal_mmapRegisterRead (int fd, uint32_t *val, uint32_t count, uint32_t offset) __attribute__((weak));
extern void  Osal_mmapRegisterWrite (int fd, uint32_t *val, uint32_t count, uint32_t offset) __attribute__((weak));
extern void* Osal_mmapMalloc (uint32_t num_bytes) __attribute__((weak));
extern void  Osal_mmapFree (void *ptr, uint32_t size) __attribute__((weak));

#define mmap_osalRegisterRead         Osal_mmapRegisterRead
#define mmap_osalRegisterWrite        Osal_mmapRegisterWrite
#define mmap_osalMalloc               Osal_mmapMalloc
#define mmap_osalFree                 Osal_mmapFree

#endif /* __KEYSTONE_MMAP_OSAL_H__ */
/* nothing past this point */

