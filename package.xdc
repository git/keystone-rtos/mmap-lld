/******************************************************************************
 * FILE PURPOSE: Package specification file 
 ******************************************************************************
 * FILE NAME: package.xdc
 *
 * DESCRIPTION: 
 *  This file contains the package specification for the MMAP Library
 *
 * Copyright (C) 2013-2018 Texas Instruments, Inc.
 *****************************************************************************/

package ti.runtime.mmap[2, 0, 0, 8] {
}

